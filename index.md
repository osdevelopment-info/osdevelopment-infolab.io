---
layout: default
---
# osdevelopment.info

osdevelopment.info is an information project on assembly programming and OS development.

The OS development focuses not on the basics but on advanced topics, especially on security.

## Projects

* [Meltdown and Spectre Samples](https://meltdown.osdevelopment.info/)

## Tutorial

The tutorial consists of several parts

* [x86 Tutorial](https://tutorial-x86.osdevelopment.info/)
* [a system emulator](https://sys-emu.osdevelopment.info/)
* [emulator BIOS](https://emu-bios.osdevelopment.info/)
